
import axios from 'axios';

export default class CurrencyClient {
    static url = "http://calc-backend";
    static urlDaily = "/api/eurofxref_daily";
    static urlSaveHistory = "/api/add_to_history";
    static urlGetHistory = "/api/gethistory";

    static rawData = null;

    static countries = [
        { currency: 'DZD', name: 'Algerian dinar' },
        { currency: 'ARS', name: 'Argentine peso' },
        { currency: 'AUD', name: 'Australian dollar' },
        { currency: 'ATS', name: 'Austrian schilling' },
        { currency: 'BEF', name: 'Belgian franc' },
        { currency: 'BRL', name: 'Brazilian real' },
        { currency: 'BGN', name: 'Bulgarian lev' },
        { currency: 'CAD', name: 'Canadian dollar' },
        { currency: 'CNY', name: 'Chinese yuan renminbi' },
        { currency: 'HRK', name: 'Croatian kuna' },
        { currency: 'CYP', name: 'Cyprus pound' },
        { currency: 'CZK', name: 'Czech koruna' },
        { currency: 'DKK', name: 'Danish krone' },
        { currency: 'EEK', name: 'Estonian kroon' },
        { currency: 'EUR', name: 'Euro' },
        { currency: 'FIM', name: 'Finnish markka' },
        { currency: 'FRF', name: 'French franc' },
        { currency: 'DEM', name: 'German mark' },
        { currency: 'GRD', name: 'Greek drachma' },
        { currency: 'HKD', name: 'Hong Kong dollar' },
        { currency: 'HUF', name: 'Hungarian forint' },
        { currency: 'ISK', name: 'Iceland krona' },
        { currency: 'INR', name: 'Indian rupee' },
        { currency: 'IDR', name: 'Indonesian rupiah' },
        { currency: 'IEP', name: 'Irish pound' },
        { currency: 'ILS', name: 'Israeli shekel' },
        { currency: 'ITL', name: 'Italian lira' },
        { currency: 'JPY', name: 'Japanese yen' },
        { currency: 'KRW', name: 'Korean won (Republic)' },
        { currency: 'LVL', name: 'Latvian lats' },
        { currency: 'LTL', name: 'Lithuanian litas' },
        { currency: 'LUF', name: 'Luxembourg franc' },
        { currency: 'MYR', name: 'Malaysian ringgit' },
        { currency: 'MTL', name: 'Maltese lira' },
        { currency: 'MXN', name: 'Mexican peso' },
        { currency: 'MAD', name: 'Moroccan dirham' },
        { currency: 'NLG', name: 'Netherlands guilder' },
        { currency: 'TWD', name: 'New Taiwan dollar' },
        { currency: 'NZD', name: 'New Zealand dollar' },
        { currency: 'NOK', name: 'Norwegian krone' },
        { currency: 'PHP', name: 'Philippine peso' },
        { currency: 'PLN', name: 'Polish zloty' },
        { currency: 'PTE', name: 'Portuguese escudo' },
        { currency: 'RON', name: 'Romanian leu' },
        { currency: 'RUB', name: 'Russian rouble' },
        { currency: 'SGD', name: 'Singapore dollar' },
        { currency: 'SKK', name: 'Slovak koruna' },
        { currency: 'SIT', name: 'Slovenian tolar' },
        { currency: 'ZAR', name: 'South African rand' },
        { currency: 'ESP', name: 'Spanish peseta' },
        { currency: 'SEK', name: 'Swedish krona' },
        { currency: 'CHF', name: 'Swiss franc' },
        { currency: 'THB', name: 'Thai baht' },
        { currency: 'TRY', name: 'Turkish lira' },
        { currency: 'GBP', name: 'UK pound sterling' },
        { currency: 'USD', name: 'US dollar' }];

    /*
    * Pulls XML data from API
    */
    static async getXML(run) {
        // Used cached data if exist
        if (this.rawData !== null) {
            run(this.rawData);
            return;
        }

        return await axios.get(this.url + this.urlDaily).then(res => {
            console.log("API call send");
            this.rawData = res.data;
            run(this.rawData);

            return this.rawData;
        }, (error) => {
            console.log(error);
        });
    }

    /*
    * Convert currency JSON to array
    */
    static async getCurrencyArray(run) {
        let tmp = [];

        return await this.getXML(data => {
            let list = Object.values(data.currencies);
            tmp = list.map(elm => { return Object.values(elm) });
            run(tmp);
            return tmp;
        });
    }

    static async getHistory(run) {
        return await axios.get(this.url + this.urlGetHistory).then(res => {
            console.log("API call getHistory");

            if (run !== undefined)
                run(res.data);

            return res.data;
        }, (error) => {
            console.log(error);
        });
    }
    /**
     * Get sample date
     */
    static getSampleDate() {
        return this.rawData ? this.rawData.date : null;
    }

    /**
     * Get currency by name
     * @param {*} name 
     */
    static getCurrency(name) {
        let tmp = Object.values(this.rawData.currencies);
        return tmp.find(elm => elm.currency === name);
    }

    /**
     * Get country name by name
     * @param {*} name 
     */
    static getCountryName(name) {
        let country = this.countries.find(elm => elm.currency === name);
        return country ? country.name : "";
    }

    /**
     * Save data to history
     * @param {*} data 
     */
    static async saveToHistory(data, run) {
        let postData = JSON.stringify(data);

        return await axios.post(this.url + this.urlSaveHistory, postData).then(res => {
            run()
        }, (error) => {
            console.log(error);
        });
    }
}