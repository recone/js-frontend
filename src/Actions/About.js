
import { React, Component } from "react";

export default class App extends Component {
    render() {
        return (
            <div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1 className="display-4">About</h1>
                <p className="lead">
                    Advanced Currency Calculator <br />
                    <small>Currences pulled from European Central Bank</small><br /><br />
            Rafal Leśniewski (c) 2020
            </p>
            </div>);
    }
}
