
import { React, Component } from "react";
import CurrencyClient from "../lib/CurrencyClient";

export default class History extends Component {
    constructor(props) {
        super(props);

        this.state = {
            history: null
        }
    }

    componentDidMount() {
        CurrencyClient.getHistory((data) => {
            let list = Object.values(data ? data : []);
            this.setState({ history: list });
        });
    }

    render() {
        return (<div className="pricing-header2 px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h1 className="display-4">History</h1>
            { this.state.history ?
            <table className="table">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col" >Source</th>
                        <th scope="col" >Amount</th>
                        <th scope="col" >Destination</th>
                        <th scope="col" >Result</th>
                        <th scope="col" className="d-none d-lg-block" >Note</th>
                        <th scope="col" >Date</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.history.map((elm) => {
                            return (
                                <tr key={elm.id} >
                                    <td >{elm.SourceRate} {elm.SourceCurrency}</td>
                                    <td >{elm.SourceAmount}</td>
                                    <td >{elm.DestinationCurrency}</td>
                                    <td >{elm.CalculatedAmount} {elm.DestinationCurrency}</td>
                                    <td className="d-none d-lg-block" ><p className="block-size">{elm.Note}</p></td>
                                    <td >{new Date(elm.created_at).toLocaleString()}</td>
                                </tr>);
                        })
                    }
                </tbody>
                </table> : <div className="spinner-border" role="status"><span className="sr-only">Loading...</span></div> }
        </div>);
    }
}
