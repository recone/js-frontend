
import { React, Component } from "react";
import CurrencyClient from "../lib/CurrencyClient";
import Success from '../lib/Success'

export default class Calculator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: "",

            currencyList: null,
            sourceCurrency: "",
            sourceCurrencyVal: "",

            destinationCurrency: "",
            destinationCurrencyVal: "",

            calculationResult: "",
            note: "",
            successModal: false,
            sampleDate: ""

        };
    }

    componentDidMount() {
        CurrencyClient.getCurrencyArray(data => {
            this.setState({ currencyList: data, sampleDate: CurrencyClient.getSampleDate() });
        })
    }
    /**
     * Change note state
     * @param {*} e 
     */
    changeNote(e) {
        this.setState({
            note: e.target.value
        });
    }
    /**
     * Calculate result
     */
    calculateResult() {
        if (!this.state.amount || !this.state.sourceCurrencyVal || !this.state.destinationCurrencyVal) {
            this.setState({ calculationResult: "" });
            return;
        }

        let calc = this.state.amount / this.state.sourceCurrencyVal * this.state.destinationCurrencyVal;
        this.setState({ calculationResult: calc ? calc.toFixed(2) : "" });
    }
    /**
     * Save calculation to history
     * @param {*} e 
     */
    saveToHistory(e) {
        let post = {
            amount: this.state.amount,

            sourceCurrency: this.state.sourceCurrency,
            sourceCurrencyVal: this.state.sourceCurrencyVal,

            destinationCurrency: this.state.destinationCurrency,
            destinationCurrencyVal: this.state.destinationCurrencyVal,

            calculationResult: this.state.calculationResult,
            note: this.state.note
        };

        CurrencyClient.saveToHistory(post, () => {
            this.setState({ successModal: true });
        });
    }
    /**
     * Change amount to calculate
     * @param {*} e 
     */
    changeAmount(e) {
        let amount = e.target.value ? parseFloat(e.target.value) : "";
        if (amount < 0) amount = "";
        this.setState({
            amount: amount
        }, function () {
            this.calculateResult();
        });
    }
    /**
     * Catch dest. currenct change
     * @param {*} e 
     */
    changeDestinationCurrency(e) {
        let val = e.target.value ? e.target.value : "";
        this.setState({
            destinationCurrency: val,
            destinationCurrencyVal: val !== "" ? parseFloat(CurrencyClient.getCurrency(val).rate) : ""
        }, function () {
            this.calculateResult();
        });
    }
    /**
     * Change source current state
     * @param {*} e 
     */
    changeSourceCurrency(e) {
        let val = e.target.value ? e.target.value : "";
        this.setState({
            sourceCurrency: val,
            sourceCurrencyVal: val !== "" ? parseFloat(CurrencyClient.getCurrency(val).rate) : ""
        }, function () {
            this.calculateResult();
        });
    }
    /**
     * Reset calculator state
     */
    resetCalculator() {
        this.setState({
            amount: "",
            sourceCurrency: "",
            sourceCurrencyVal: "",

            destinationCurrency: "",
            destinationCurrencyVal: "",

            calculationResult: "",
            note: "",
            successModal: false
        });
    }

    render() {
        return (
            <div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                { !this.state.successModal ?
                    <div className="card-deck mb-12 text-center">
                        <div className="card mb-12 box-shadow">
                            <div className="card-header">
                                <h4 className="my-0 font-weight-normal">Calculate currency</h4>
                            </div>
                            <div className="card-body">
                                {this.state.currencyList ? <div className="text-left" >
                                    <div className="form-row">
                                        <div className="form-group col-md-5">
                                            <label>Data source: <b>European Central Bank</b></label>
                                        </div>
                                        <div className="form-group col-md-7">
                                            <label>Date of the sample: <b>{this.state.sampleDate}</b></label>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-group col-md-3">
                                            <label htmlFor="inputCurr4">Source currency</label>
                                            <select className="form-control" value={this.state.sourceCurrency} onChange={this.changeSourceCurrency.bind(this)}>
                                                <option value="">Select one...</option>
                                                {this.state.currencyList.map((text, i) => (
                                                    <option key={i} value={text[0]} >
                                                        {text[0]}
                                                    </option>
                                                ))}
                                            </select>
                                            <small>{CurrencyClient.getCountryName(this.state.sourceCurrency)}</small>
                                        </div>
                                        <div className="form-group col-md-9">
                                            <label htmlFor="inputAmount">Amount</label>
                                            <input type="number" className="form-control" id="inputAmount" placeholder="Amount" value={this.state.amount} onChange={this.changeAmount.bind(this)} />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-group col-md-3">
                                            <label htmlFor="selectDestCurr">Destination currency</label>
                                            <select id="selectDestCurr" className="form-control" value={this.state.destinationCurrency} onChange={this.changeDestinationCurrency.bind(this)} >
                                                <option value="">Select one...</option>
                                                {this.state.currencyList.map((text, i) => (
                                                    <option key={i} value={text[0]}>
                                                        {text[0]}
                                                    </option>
                                                ))}
                                            </select>
                                            <small>{CurrencyClient.getCountryName(this.state.destinationCurrency)}</small>
                                        </div>
                                        <div className="form-group col-md-5">
                                            <label htmlFor="inputResult">Result</label>
                                            <input disabled type="number" className={this.state.calculationResult ? 'form-control is-valid' : 'form-control'} id="inputResult" placeholder="Result" value={this.state.calculationResult} />
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label htmlFor="inputRate">Rate to EURO</label>
                                            <input value={this.state.sourceCurrencyVal ? "1 EURO = " + this.state.sourceCurrencyVal.toFixed(2) + " " + this.state.sourceCurrency : ""} disabled type="text" className="form-control" id="inputRate" placeholder="Rate" />
                                        </div>
                                    </div>
                                    {this.state.calculationResult && this.state.calculationResult !== "NaN" ?
                                        <div className="form-row">
                                            <div className="form-group col-md-12">
                                                <label htmlFor="texareaNote">Note</label>
                                                <textarea id="texareaNote" maxLength="90" className="form-control" rows="3" onChange={this.changeNote.bind(this)} value={this.state.note} ></textarea>
                                            </div>
                                            <button className="btn btn-primary" onClick={this.saveToHistory.bind(this)} >Save to history</button>
                                        </div> : ""}
                                </div> : <div className="spinner-border" role="status"><span className="sr-only">Loading...</span></div>}
                            </div>
                        </div>
                    </div> : <Success reset={() => this.resetCalculator()} />}
            </div>

        );
    }
}
