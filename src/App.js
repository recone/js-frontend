import React from "react";
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import About from './Actions/About';
import History from './Actions/History';
import Calculator from './Actions/Calculator';

import Footer from './Footer';

export default function App() {
  return (
    <Router>
      <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <h5 className="my-0 mr-md-auto font-weight-normal">Advanced Currency Calculator</h5>
        <nav className="my-2 my-md-0 mr-md-3">
          <Link className="p-2 text-dark" to="/" >Calculator</Link>
          <Link className="p-2 text-dark" to="/history" >History</Link>
          <Link className="p-2 text-dark" to="/about" >About</Link>
        </nav>
      </div>

      <div className="container">
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/history">
            <History />
          </Route>
          <Route path="/">
            <Calculator />
          </Route>
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}