
import { React, Component } from "react";

export default class Footer extends Component {
    render() {
        return (
            <footer className="pt-4 my-md-5 pt-md-5 border-top">
                <div className="row">
                    <div className="col-12 col-md">
                        <small className="d-block mb-3 text-muted">&copy; 2020</small>
                    </div>
                </div>
            </footer>
        );
    }
}
